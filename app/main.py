import math
import os
import sys
import time
import ctypes
import psycopg2
import geocoder

import numpy as np

from datetime import datetime
from PyQt5.QtCore import QSize, QSettings, Qt
from PyQt5.QtWidgets import (
    QPushButton, QVBoxLayout, QHBoxLayout, QLabel, QMessageBox, QApplication, QWidget, QLineEdit,
    QMainWindow, QTableWidget, QTableWidgetItem, QFileDialog, QPlainTextEdit
)

from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar

from app.charts import PlotCanvas
from app.load_data_thread import LoadDataThread, LoadDataWindowThread

APP_NAME = 'WPOJEE Project Active Power'
AUTHORS = 'Dobrosława Kolasińska and Dawid Kopeć'
TIME = {
    3: '3s',
    600: '10min',
    7200: '2h'
}
START_APP_TIME = time.time()


def resource_path(relative_path):
    """ Get absolute path to resource, works for dev and for PyInstaller """
    try:
        # PyInstaller creates a temp folder and stores path in _MEIPASS
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath("..")

    return os.path.join(base_path, relative_path)


class Window(QMainWindow):

    def __init__(self, parent=None):
        super(Window, self).__init__(parent)

        self.parent = parent

        # app_icon = QIcon()
        # app_icon.addFile(resource_path('.\\Icons\\drone.ico'))
        # self.setWindowIcon(app_icon)

        self.conn = psycopg2.connect(
            database="wejmaksr",
            user="wejmaksr",
            password="BwjA0H1QZjfxY6HeB0tyH0GrZmi3CQqq",
            host="rogue.db.elephantsql.com",
            port="5432"
        )

        self.login = os.getlogin()
        g = geocoder.ip('me')
        self.geo_data = g.current_result
        self.cursor = self.conn.cursor()

        self.setFixedSize(1300, 800)

        self.canvas = PlotCanvas(self, width=5, height=4)
        self.toolbar = NavigationToolbar(self.canvas, self, coordinates=False)
        self.time = None
        self.voltage_data = None
        self.current_data = None
        self.zero_crosses = None
        self.sample_rate = 10240
        self.unit_power = list()

        self.settings = QSettings('settings.ini', QSettings.IniFormat)
        self.file_path = dict(
            # voltage='D:\\Politechnika\\MGR\\WPOJEE\\Projekt\\Temat_11_Moc_Czynna\\u_LED_2W.csv',
            # current='D:\\Politechnika\\MGR\\WPOJEE\\Projekt\\Temat_11_Moc_Czynna\\i_LED_2W.csv'
            voltage='',
            current=''
        )

        self.csv_voltage_file_path_label = QLabel('Input file voltage:')
        self.csv_voltage_file_path_label.setMaximumWidth(150)
        self.csv_current_file_path_label = QLabel('Input file current:')
        self.csv_current_file_path_label.setMaximumWidth(150)

        self.button_voltage_select_file = QPushButton('Select voltage file')
        self.button_voltage_select_file.clicked.connect(lambda: self.select_file('voltage'))
        self.button_voltage_select_file.setMaximumWidth(100)
        self.line_edit_voltage_file_path = QLineEdit()
        self.line_edit_voltage_file_path.setText(self.file_path['voltage'])
        self.line_edit_voltage_file_path.setReadOnly(True)

        self.button_current_select_file = QPushButton('Select current file')
        self.button_current_select_file.clicked.connect(lambda: self.select_file('current'))
        self.button_current_select_file.setMaximumWidth(100)
        self.line_edit_current_file_path = QLineEdit()
        self.line_edit_current_file_path.setText(self.file_path['current'])
        self.line_edit_current_file_path.setReadOnly(True)

        self.button_load_data = QPushButton('Load data')
        self.button_load_data.clicked.connect(self.load_data)

        self.button_calculate_unit_active_power = QPushButton('Calculate unit active power')
        self.button_calculate_unit_active_power.clicked.connect(self.calculate_unit_active_power)

        self.button_calculate_3s_aggregation = QPushButton('Calculate 3s aggregation')
        self.button_calculate_3s_aggregation.clicked.connect(lambda: self.calculate_aggregation(3))

        self.button_calculate_10min_aggregation = QPushButton('Calculate 10min aggregation')
        # self.button_calculate_10min_aggregation.clicked.connect(lambda: self.calculate_aggregation(600))
        self.button_calculate_10min_aggregation.clicked.connect(self.calculate_10min_aggregation)

        self.button_calculate_2h_aggregation = QPushButton('Calculate 2h aggregation')
        self.button_calculate_2h_aggregation.clicked.connect(lambda: self.calculate_aggregation(7200))

        self.column_labels = ['Time [ms]', 'Voltage [V]', 'Current [A]', 'Zero Cross']

        self.table_input_data = QTableWidget()
        self.table_input_data.setColumnCount(len(self.column_labels))
        self.table_input_data.setHorizontalHeaderLabels(self.column_labels)
        for i, _ in enumerate(self.column_labels):
            self.table_input_data.setColumnWidth(i, 100)
        self.table_input_data.setEditTriggers(QTableWidget.NoEditTriggers)
        self.table_input_data.setMinimumWidth(101 * len(self.column_labels) + len(self.column_labels) + 47)
        self.table_input_data.setRowCount(1000)

        zero_cross_col_idx = self.column_labels.index('Zero Cross')
        for i in range(1000):
            self.table_input_data.setItem(i, zero_cross_col_idx, QTableWidgetItem())

        self.label_authors = QLabel('Authors: ' + AUTHORS)
        self.label_authors.setFixedSize(QSize(self.width(), 20))

        self.load_data_window = QWidget()
        self.label_load_data_info = QLabel()
        self.label_load_data_info.setAlignment(Qt.AlignCenter)

        self.calculate_unit_power_window = QWidget()
        self.text_box_calculate_unit_power = QPlainTextEdit()
        self.text_box_calculate_unit_power.setReadOnly(True)

        self.calculate_aggregated_power_window = QWidget()
        self.text_box_aggregated_power = QPlainTextEdit()
        self.text_box_aggregated_power.setReadOnly(True)

        self.load_data_thread = LoadDataThread(
            self.column_labels,
            self.file_path,
            self.table_input_data,
            self.sample_rate,
            self.canvas,
            self.log_database
        )

        self.load_data_thread_window = LoadDataWindowThread(
            self.load_data_thread._signal, self.label_load_data_info, self.load_data_window
        )
        self.load_data_thread._signal.connect(self.load_data_signal_accept)

        self.setWindowTitle(APP_NAME)

        self.refresh_layout()
        self.show()
        elapsed_time = round(time.time() - START_APP_TIME, 2)
        self.log_database(msg='Turn on app', elapsed_time=elapsed_time)

    def refresh_layout(self):
        columns = list()

        column = QVBoxLayout()

        row = QHBoxLayout()
        row.addWidget(self.table_input_data)
        column.addLayout(row)

        columns.append(column)

        column = QVBoxLayout()

        row = QHBoxLayout()
        row.addWidget(self.csv_voltage_file_path_label)
        row.addWidget(self.line_edit_voltage_file_path)
        row.addWidget(self.button_voltage_select_file)
        column.addLayout(row)

        row = QHBoxLayout()
        row.addWidget(self.csv_current_file_path_label)
        row.addWidget(self.line_edit_current_file_path)
        row.addWidget(self.button_current_select_file)
        column.addLayout(row)

        row = QHBoxLayout()
        row.addWidget(self.button_load_data)
        row.addWidget(self.button_calculate_unit_active_power)
        row.addWidget(self.button_calculate_3s_aggregation)
        row.addWidget(self.button_calculate_10min_aggregation)
        row.addWidget(self.button_calculate_2h_aggregation)
        column.addLayout(row)

        row = QVBoxLayout()
        row.addWidget(self.toolbar, alignment=Qt.AlignCenter | Qt.AlignTop)
        row.addWidget(self.canvas)
        column.addLayout(row)

        row = QHBoxLayout()
        row.addWidget(self.label_authors)
        column.addLayout(row)

        columns.append(column)

        panel = QHBoxLayout()
        for column in columns:
            panel.addLayout(column)

        window = QWidget()
        window.setLayout(panel)

        self.setCentralWidget(window)

        load_data_info = QVBoxLayout()
        load_data_info.addWidget(self.label_load_data_info)

        self.load_data_window.setLayout(load_data_info)
        self.load_data_window.setFixedSize(250, 150)
        self.load_data_window.setWindowTitle('Load data')

        calculate_unit_power = QVBoxLayout()
        calculate_unit_power.addWidget(self.text_box_calculate_unit_power)

        self.calculate_unit_power_window.setLayout(calculate_unit_power)
        self.calculate_unit_power_window.setFixedSize(500, 800)
        self.calculate_unit_power_window.setWindowTitle('Unit power')

        aggregated_power = QVBoxLayout()
        aggregated_power.addWidget(self.text_box_aggregated_power)

        self.calculate_aggregated_power_window.setLayout(aggregated_power)
        self.calculate_aggregated_power_window.setFixedSize(500, 800)

    def select_file(self, data_type: str):
        frame = QFileDialog.getOpenFileName(
            self, 'Select file', self.settings.value('path', ''), "CSVFile (*.csv)"
        )

        self.file_path.update({data_type: frame[0]})
        self.settings.setValue('path', os.path.dirname(os.path.abspath(self.file_path[data_type])))
        self.settings.sync()
        if data_type == 'voltage':
            self.line_edit_voltage_file_path.setText(self.file_path[data_type])
        elif data_type == 'current':
            self.line_edit_current_file_path.setText(self.file_path[data_type])
        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error: Unknown data type")
            msg.setWindowTitle("Error")
            msg.exec_()

    def load_data(self):

        if self.file_path['voltage'] == '' or self.file_path['current'] == '':
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error: Files not selected")
            msg.setWindowTitle("Error")
            msg.exec_()
            return

        self.load_data_window.show()

        self.load_data_thread_window.start()
        self.load_data_thread.start()

    def load_data_signal_accept(self, msg: dict):
        if msg['status'] and msg['msg'] == '':
            self.time = msg['time']
            self.voltage_data = msg['voltage_data']
            self.current_data = msg['current_data']
            self.zero_crosses = msg['zero_crosses']

    def calculate_unit_active_power(self, silent=False):
        start_time = time.time()

        if self.voltage_data is None or self.current_data is None:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error: Load data first please")
            msg.setWindowTitle("Error")
            msg.exec_()
            return

        power = self.voltage_data * self.current_data
        self.unit_power = list()

        zero_crosses = self.zero_crosses[:, 0]

        for partition in range(20, zero_crosses.shape[0], 20):
            first_idx = zero_crosses[partition-20]
            last_idx = zero_crosses[partition]
            n = last_idx - first_idx
            self.unit_power.append(float(power[first_idx:last_idx].sum() / n))

        if not silent:
            self.text_box_calculate_unit_power.setPlainText(str(self.unit_power))
            self.calculate_unit_power_window.show()

        elapsed_time = round(time.time() - start_time, 2)

        self.log_database(msg='Calculate Unit Active Power', elapsed_time=elapsed_time)

        print('calculate_unit_active_power: {}[s]'.format(elapsed_time))

    def calculate_aggregation(self, aggregation_time):
        start_time = time.time()

        samples = int(aggregation_time * 1 / (10 * 1 / 50))

        if not len(self.unit_power):
            self.calculate_unit_active_power(silent=True)

        unit_power = np.array(self.unit_power)

        aggregated_powers = list()
        for partition in range(samples, unit_power.shape[0], samples):
            aggregated_power = math.sqrt(1 / samples * (unit_power[partition-samples:partition] ** 2).sum())
            aggregated_powers.append(aggregated_power)

        self.calculate_aggregated_power_window.setWindowTitle('Aggregated power {}'.format(TIME[aggregation_time]))
        self.text_box_aggregated_power.setPlainText(str(aggregated_powers))
        self.calculate_aggregated_power_window.show()

        elapsed_time = round(time.time() - start_time, 2)

        self.log_database(msg='Calculate {}s Aggregation'.format(aggregation_time), elapsed_time=elapsed_time)

        print('calculate_aggregation: {}[s]'.format(elapsed_time))

    def calculate_10min_aggregation(self):
        start_time = time.time()

        if self.voltage_data is None or self.current_data is None:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error: Load data first please")
            msg.setWindowTitle("Error")
            msg.exec_()
            return

        power = self.voltage_data * self.current_data

        zero_crosses = self.zero_crosses[:, 0]

        # 10 min aggregate
        samples_count_10min = 600 * self.sample_rate
        zero_crosses_count_unit_power = 20
        # aggregated_10min_powers = np.zeros(math.ceil(unit_powers_sample_idx[-1] / (600 * self.sample_rate)))
        aggregated_10min_powers = list()
        unit_power = list()
        # podział na okresy 10 min
        for period_idx in range(samples_count_10min, power.shape[0], samples_count_10min):

            last_unit_power_idx = np.where(zero_crosses <= period_idx)[0].shape[0]
            # sprawdzenie czy przedziały muszą się nałożyć
            if last_unit_power_idx + zero_crosses_count_unit_power != period_idx:
                # nakładka
                if last_unit_power_idx + zero_crosses_count_unit_power <= zero_crosses.shape[0]:
                    last_unit_power_idx += zero_crosses_count_unit_power

            # obliczenie mocy jednostkowych (10 okresów) w danym przedziale 10 minutowym
            first_unit_power_idx = zero_crosses_count_unit_power + period_idx - samples_count_10min
            for partition in range(first_unit_power_idx, last_unit_power_idx, zero_crosses_count_unit_power):
                first_idx = zero_crosses[partition - zero_crosses_count_unit_power]
                last_idx = zero_crosses[partition]
                n = last_idx - first_idx
                unit_power.append(float(power[first_idx:last_idx].sum() / n))

            # agregacja mocy 10 minutowej zgodnie ze wzorem na podstawie listy mocy jednostkowych
            unit_power = np.array(unit_power)
            aggregate_power = math.sqrt(1 / unit_power.shape[0] * (unit_power ** 2).sum())
            aggregated_10min_powers.append(aggregate_power)

        self.calculate_aggregated_power_window.setWindowTitle('Aggregated power 10 min')
        self.text_box_aggregated_power.setPlainText(str(aggregated_10min_powers))
        self.calculate_aggregated_power_window.show()

        elapsed_time = round(time.time() - start_time, 2)

        self.log_database(msg='Calculate 10min Aggregation', elapsed_time=elapsed_time)

        print('calculate_aggregation: {}[s]'.format(elapsed_time))

    def log_database(self, msg, elapsed_time):
        self.cursor.execute(
            "INSERT INTO log (Data, Login, Localization, IP, Type, Time) "
            "VALUES ('{}', '{}', '{}', '{}', '{}', '{}')".format(
                datetime.now(),
                self.login,
                self.geo_data.address,
                self.geo_data.ip,
                msg,
                elapsed_time
            )
        )
        self.conn.commit()


if __name__ == '__main__':
    app = QApplication(sys.argv)

    myappid = 'mycompany.myproduct.subproduct.version'  # arbitrary string
    ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    main = Window()
    sys.exit(app.exec_())
