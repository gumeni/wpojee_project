import time

import numpy as np
import pandas as pd

from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtGui import QBrush, QColor
from PyQt5.QtWidgets import QTableWidgetItem, QMessageBox


class LoadDataThread(QThread):
    _signal = pyqtSignal(object)

    def __init__(self, column_labels, file_path, table_input_data, sample_rate, canvas, log_database):
        super(LoadDataThread, self).__init__()
        self.column_labels = column_labels
        self.file_path = file_path
        self.table_input_data = table_input_data
        self.sample_rate = sample_rate
        self.canvas = canvas
        self.log_database = log_database

    def run(self):
        self.load_data()

    def load_data(self):
        start_time = time.time()
        try:
            self._signal.emit(dict(msg='Loading voltage file', status=False))
            voltage_data = np.array(self.read_data_from_csv_file(self.file_path['voltage']))
            self._signal.emit(dict(msg='Loading current file', status=False))
            current_data = np.array(self.read_data_from_csv_file(self.file_path['current']))
            self._signal.emit(dict(msg='Finding out zero crosses', status=False))
            zero_crosses = self.find_zero_crosses(voltage_data)

            self._signal.emit(dict(msg='Inserting first 1000 records to table', status=False))
            time_col_idx = self.column_labels.index('Time [ms]')
            voltage_col_idx = self.column_labels.index('Voltage [V]')
            current_col_idx = self.column_labels.index('Current [A]')
            zero_cross_col_idx = self.column_labels.index('Zero Cross')
            data = enumerate(zip(voltage_data, current_data))
            sample_period = 1 / self.sample_rate * 1000
            for idx, (voltage, current) in data:
                self.table_input_data.setItem(
                    idx, time_col_idx, QTableWidgetItem(str(round(idx * sample_period, 5)))
                )
                self.table_input_data.setItem(idx, voltage_col_idx, QTableWidgetItem(str(voltage)))
                self.table_input_data.setItem(idx, current_col_idx, QTableWidgetItem(str(current)))
                self.table_input_data.item(idx, zero_cross_col_idx).setBackground(QBrush(QColor(255, 0, 0)))
                if idx == 999:
                    break

            last_zero_cross_idx = 0
            for last_zero_cross_idx, zero_cross in enumerate(zero_crosses):
                if zero_cross[1] > 999:
                    break
                self.table_input_data.item(zero_cross[0], zero_cross_col_idx).setBackground(QBrush(QColor(0, 255, 0)))
                self.table_input_data.item(zero_cross[1], zero_cross_col_idx).setBackground(QBrush(QColor(0, 255, 0)))

            for i, _ in enumerate(self.column_labels):
                self.table_input_data.setColumnWidth(i, 100)

            self._signal.emit(dict(msg='Drawing chart from first 1000 records', status=False))
            start_time_plot = time.time()
            time_ = np.arange(voltage_data.shape[0]) * sample_period
            zero_crosses_reduced = zero_crosses[:last_zero_cross_idx]
            idxes_zero_cross = np.reshape(zero_crosses_reduced, -1)
            voltage_data_zero_cross = voltage_data[idxes_zero_cross]
            voltage_data_zero_cross = np.reshape(voltage_data_zero_cross, (voltage_data_zero_cross.shape[0] // 2, 2))
            time_zero_cross = time_[idxes_zero_cross]
            time_zero_cross = np.reshape(time_zero_cross, (time_zero_cross.shape[0] // 2, 2))
            coefficients = [np.polyfit(x, y, 1) for x, y in zip(time_zero_cross, voltage_data_zero_cross)]
            time_zero_cross_estimated = [-b/a for a, b in coefficients]
            # time_zero_cross = np.array(zero_crosses_reduced).astype(dtype='float64') * sample_period
            self.canvas.clear_plot()
            self.canvas.init_plot()
            self.canvas.plot(
                time_[:1000],
                voltage_data[:1000],
                current_data[:1000],
                time_zero_cross_estimated,
                np.zeros(len(time_zero_cross_estimated))
            )
            print('plot: {}[s]'.format(round(time.time() - start_time_plot, 2)))
            self._signal.emit(
                dict(
                    msg='',
                    time=time_,
                    voltage_data=voltage_data,
                    current_data=current_data,
                    zero_crosses=zero_crosses,
                    status=True
                )
            )

            elapsed_time = round(time.time() - start_time, 2)

            self.log_database(msg='Load Data', elapsed_time=elapsed_time)
            self._signal.emit(dict(msg='Load data finished', status=True))
        except Exception as err:
            print('Error: {}'.format(err))
            self._signal.emit(dict(msg='Error: {}'.format(err), status=False))
            elapsed_time = round(time.time() - start_time, 2)
            self.log_database(msg='Load Data Error: {}'.format(err), elapsed_time=elapsed_time)
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error: {}".format(err))
            msg.setWindowTitle("Error")
            msg.exec_()

    @staticmethod
    def read_data_from_csv_file(path: str):
        start_time = time.time()
        data = pd.read_csv(path, usecols=[0], header=None)[0]
        print('read_data_from_csv_file: {}[s]'.format(round(time.time() - start_time, 2)))
        return data

    @staticmethod
    def find_zero_crosses(data: np.array):
        start_time = time.time()
        data_shifted = np.array(data[1:])
        data = np.array(data[:-1])
        zero_crosses_first_point = np.where(data * data_shifted <= 0)[0]
        zero_crosses_second_point = zero_crosses_first_point + 1
        zero_crosses = np.column_stack((zero_crosses_first_point, zero_crosses_second_point))
        print('find_zero_crosses_time: {}[s]'.format(round(time.time() - start_time, 2)))
        return zero_crosses


class LoadDataWindowThread(QThread):

    def __init__(self, signal, label_load_data_info, load_data_window):
        super(LoadDataWindowThread, self).__init__()
        self.signal = signal
        self.signal.connect(self.load_data_signal)
        self.label_load_data_info = label_load_data_info
        self.running = True
        self.load_data_window = load_data_window

    def run(self):
        while self.running:
            time.sleep(0.1)

    def load_data_signal(self, msg):
        if msg['msg'] != '':
            self.label_load_data_info.setText(msg['msg'])
            if msg['status']:
                self.running = False
                self.load_data_window.hide()
