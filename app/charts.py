
from PyQt5.QtWidgets import QSizePolicy

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure


class PlotCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self, QSizePolicy.Expanding, QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

        self.ax = None
        self.line_voltage = None
        self.dot_zero_cross = None
        self.line_current = None
        self.ax2 = None

        # self.ax_bar = None

        self.init_plot()

    def init_plot(self):

        self.ax = self.figure.add_subplot(111)
        self.line_voltage, = self.ax.plot([], [], 'b-', label='Voltage')
        self.ax.set_xlabel('Time [ms]')
        self.ax.set_ylabel('Voltage [V]')
        self.ax.yaxis.label.set_color(self.line_voltage.get_color())

        self.dot_zero_cross, = self.ax.plot([], [], 'r*', label='Zero Cross')

        self.ax2 = self.ax.twinx()
        self.line_current, = self.ax2.plot([], [], 'g-', label='Current')
        self.ax2.set_ylabel('Current [A]')
        self.ax2.yaxis.label.set_color(self.line_current.get_color())

        self.ax.legend(
            handles=[
                self.line_voltage,
                self.line_current,
                self.dot_zero_cross
            ],
            loc='upper center',
            bbox_to_anchor=(0.5, 1.15),
            ncol=2,
            shadow=True,
        )

    def plot(self, time, voltage, current, time_zero_cross, zero_cross):

        self.ax.legend(
            handles=[
                self.line_voltage,
                self.line_current,
                self.dot_zero_cross
            ],
            loc='upper center',
            bbox_to_anchor=(0.5, 1.15),
            ncol=2,
            shadow=True,
        )

        self.ax.plot(time, voltage, 'b-', label='Voltage')
        self.ax.plot(time_zero_cross, zero_cross, 'r*', label='Zero Cross')
        self.ax2.plot(time, current, 'g-', label='Current')

        self.draw()

    def clear_plot(self):
        self.ax.remove()
        self.ax2.remove()
        self.draw()
